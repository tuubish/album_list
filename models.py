from datetime import datetime
from peewee import *

db = SqliteDatabase('album.db')


class Artist(Model):
    artist_name = CharField(max_length=255)

    class Meta:
        database = db
        db_table = "artist"


class Album(Model):
    music_name = CharField(max_length=100)
    genre = CharField()
    released = DateField()
    artist = ForeignKeyField(Artist)

    class Meta:
        database = db
        db_table = "album"
        order_by = ("music_name",)

db.connect()
db.create_tables([Album, Artist])
